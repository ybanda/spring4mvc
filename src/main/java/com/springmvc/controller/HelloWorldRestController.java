package com.springmvc.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.springmvc.model.User;
import com.springmvc.service.UserService;

@RestController
public class HelloWorldRestController {

	@Autowired
	UserService userService;  //Service which will do all data retrieval/manipulation work

	//-------------------Retrieve Single User--------------------------------------------------------

	@RequestMapping(value = "/user/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<User> getUser(@PathVariable("id") long id,HttpServletRequest request) {
		System.out.println("Fetching User with id " + id);
		User user = userService.findById(id);
		Object name = request.getSession().getAttribute("Name");
		user.setUsername(null!=name?name.toString():"null");
		if (user == null) {
			System.out.println("User with id " + id + " not found");
			return new ResponseEntity<User>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<User>(user, HttpStatus.OK);
	}



	

	@RequestMapping(value = "/saveNameToSession", method = RequestMethod.GET)
	public ModelAndView saveNameToSession(HttpServletRequest request,@ModelAttribute("uname") String uname,@ModelAttribute("time") Integer time) {
		System.out.println("Inside of saveNameToSession");
		System.out.println("Name Entered = "+uname+" \t Time = "+time);
		HttpSession session= request.getSession();
		session.setAttribute("Name",uname); 
		session.setAttribute("Time",uname); 
		if(null!=time){
			session.setMaxInactiveInterval(Integer.parseInt(time.toString().trim()));
			System.out.println("Time Out Set :: "+time.toString());
		}
		ModelAndView view = new ModelAndView();
		view.setViewName("page1");
		System.out.println("session = "+request.getSession().getAttribute("Name")+"\t Time Out::"+session.getMaxInactiveInterval());
		return view;
	}

	

	@RequestMapping(value = "/movePage", method = RequestMethod.GET)
	public ModelAndView moveToPage1() {
		
		
		ModelAndView view = new ModelAndView();
		view.setViewName("page2");
		
		return view;
	}


}