package com.springmvc.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/")
public class IndexController {

	  @RequestMapping(method = RequestMethod.GET)
	    public String getIndexPage() {
	        return "UserManagement";
	    }
	  
	  @RequestMapping(value="/page1",method = RequestMethod.GET)
	    public String getUserPage() {
	        return "page1";
	    }
	  
	  @RequestMapping(value="/page2",method = RequestMethod.GET)
	    public String getUserPage2() {
	        return "page2";
	    }

}