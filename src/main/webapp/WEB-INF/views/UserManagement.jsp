<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html>
<head>
<title>Example</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link href="<c:url value='/static/css/app.css' />" rel="stylesheet"></link>
</head>
<body>

	<div class="panel panel-default">

		<div class="formcontainer">
			<form
				action="/SessionTimeOut/saveNameToSession">
				<div class="row">
					<div class="form-group col-md-12">
						<div class="col-md-7">
						<table>
						<tr>
						<td>Name: </td>
						<td><input type="text" name="uname"></td>
						</tr>
						<tr>
						<td>Time to keep Alive</td>
						<td><input type="text" name="time"><br></td>
						</tr>
						<tr> 
							<td colspan="2" align="center"><input
								type="submit" value="Submit" />
								</td>
						</tr>
						</table>
							 <br>
							  
							
								
						</div>
					</div>
				</div>
			</form>

		</div>
		Session Value for Name =
		<%=request.getSession().getAttribute("Name")%>
		
	</div>
	Links to check the session value entered<br>
		<a href="/SessionTimeOut/user/1" target="_blank">Link #1</a><br>
		 <a
			href="/SessionTimeOut/user/2" target="_blank">Link #2</a><br>
			 <a
			href="/SessionTimeOut/user/3" target="_blank">Link #3</a> <br>
			<a
			href="/SessionTimeOut/user/4" target="_blank">Link #4</a><br>
			 <a
			href="/SessionTimeOut/user/5" target="_blank">Link #5</a><br>
			 <a
			href="/SessionTimeOut/user/6" target="_blank">Link #6</a><br>
			 <a
			href="/SessionTimeOut/user/7" target="_blank">Link #7</a><br>
			 <a
			href="/SessionTimeOut/user/8" target="_blank">Link #8</a><br>
			 <a
			href="/SessionTimeOut/user/9" target="_blank">Link #9</a><br>
			 <a
			href="/SessionTimeOut/user/10" target="_blank">Link #10</a><br>
</body>
</html>